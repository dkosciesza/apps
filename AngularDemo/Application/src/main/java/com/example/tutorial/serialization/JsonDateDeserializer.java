package com.example.tutorial.serialization;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dominik on 2016-03-06.
 */
@Component
public class JsonDateDeserializer extends JsonDeserializer<Date> {

    @Override
    public Date deserialize(final JsonParser p, final DeserializationContext ctxt) throws IOException, JsonParseException {
        try {
            SimpleDateFormat DF = new SimpleDateFormat("yyyy/MM/dd");
            return DF.parse(p.getText());
        } catch (ParseException e) {
            throw new RuntimeException("parse date exception");
        }
    }
}
