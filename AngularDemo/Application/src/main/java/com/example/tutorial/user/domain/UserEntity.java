package com.example.tutorial.user.domain;

import com.example.tutorial.serialization.JsonDateDeserializer;
import com.example.tutorial.serialization.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

/**
 * Created by Dominik on 2016-03-06.
 */
//@Entity
public class UserEntity {
    Long id;
    String surname;
    String name;

    @JsonFormat(pattern = "yyyy/MM/dd")
    @JsonProperty("birthDate")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    Date birthDate;

    public Long getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public UserEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public UserEntity setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public UserEntity setName(String name) {
        this.name = name;
        return this;
    }

    public UserEntity setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserEntity that = (UserEntity) o;

        if (!id.equals(that.id)) return false;
        if (!surname.equals(that.surname)) return false;
        return name.equals(that.name);

    }

    @Override
    public int hashCode() { // UWAGA zły hashCode
        int result = 23124324;
        if(id!=null){
            result = id.hashCode();
        }
        result = 31 * result + surname.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
