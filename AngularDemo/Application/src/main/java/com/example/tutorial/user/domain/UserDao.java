package com.example.tutorial.user.domain;

import com.example.tutorial.message.JsonMsgResponse;
import com.google.common.collect.ImmutableMap;
import org.springframework.http.HttpStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dominik on 2016-03-06.
 */
//@Repository
public class UserDao {

    private static final SimpleDateFormat DF = new SimpleDateFormat("yyyy/MM/dd");

    private List<UserEntity> users = new ArrayList<UserEntity>() {{
        try {
            add(new UserEntity().setId(1L)
                    .setSurname("Kowalski")
                    .setName("Gienek")
                    .setBirthDate(DF.parse("1999/01/01")));
        add(new UserEntity().setId(2L)
                .setSurname("Dzika")
                .setName("Joanna")
                .setBirthDate(DF.parse("1998/02/02")));
        add(new UserEntity().setId(3L)
                .setSurname("Pierdas")
                .setName("Leszek")
                .setBirthDate(DF.parse("2016/03/03")));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }};


    /**
     * Pobierz wszystkich
     */
    public List<UserEntity> findAll(){
        return users;
    }

    /**
     * Pobierz uzytkownika po identyfikatorze
     */
    public UserEntity findOne(Long id){
        if(id > users.size() || id <= 0){
            System.out.println("findOne not exist: " + id);
            throw new RuntimeException("Nie istnieje user o id: "+ id);
        }
        UserEntity user = users.stream().filter(u -> u.getId().equals(id)).findFirst().get();
        System.out.println("findOne: " + user.toString());
        return user;
    }

    /**
     * Dodaj/zmodyfikuj użytkownika
     */
    public ImmutableMap<HttpStatus, String> addUpdateUser(UserEntity userEntity) throws ParseException{
        UserEntity user = null;
        //update
        if(userEntity.getId()!= null){
            System.out.println("addUser update: " + userEntity.toString());
            user =  users.stream().filter(u -> u.equals(userEntity)).findFirst().get();
        } else{
            System.out.println("addUser new: " + userEntity.toString());
            user = userEntity;
            user.setId(Long.valueOf(users.size()) + 1);
        }

        System.out.println("addUser success: " + user.toString());
        users.add(user);
        return JsonMsgResponse.getResponse(HttpStatus.OK, "Dodano: " + user.toString());
    }

    /**
     * Usun użytkownika o id
     */
    public ImmutableMap<HttpStatus, String> removeUser(Long id){
        if(id > users.size() || id <= 0){
            System.out.println("removeUser not exist: " + id);
            JsonMsgResponse.getResponse(HttpStatus.BAD_REQUEST, "Nie istnieje user o id: "+ id);
        }
        UserEntity userEntity = users.stream().filter(u -> u.getId().equals(id)).findFirst().get();
        users.remove(userEntity);
        System.out.println("removeUser: " + userEntity.toString());
        return JsonMsgResponse.getResponse(HttpStatus.OK, "Usunięto: "+ userEntity.toString());
    }
}
