package com.example.tutorial.user.domain;

import com.google.common.collect.ImmutableMap;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Dominik on 2016-02-29.
 */
@Service
public class UserService {

    UserDao userDao = new UserDao();


    public List<UserEntity> findAll(){
        return userDao.findAll();
    }

    /**
     * Pobierz uzytkownika po identyfikatorze
     */
    public UserEntity findOne(Long id){
        return userDao.findOne(id);
    }

    /**
     * Dodaj/zmodyfikuj uzytkownika
     */
    public ImmutableMap<HttpStatus, String> addUpdateUser(UserEntity userEntity) throws ParseException{
        return userDao.addUpdateUser(userEntity);
    }

    /**
     * Usun uzytkownika o id
     */
    public ImmutableMap<HttpStatus, String> removeUser(Long id){
        return userDao.removeUser(id);
    }
}
