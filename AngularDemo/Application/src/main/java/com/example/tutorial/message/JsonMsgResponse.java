package com.example.tutorial.message;

import com.google.common.collect.ImmutableMap;
import org.springframework.http.HttpStatus;

/**
 * Created by Dominik on 2016-03-06.
 */
public class JsonMsgResponse {

    public static ImmutableMap<HttpStatus, String> getResponse(HttpStatus status, String message){
        return ImmutableMap.of(status, message);
    }
}
