package com.example.tutorial.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dominik on 2016-03-06.
 */
@Component
public class JsonDateSerializer extends JsonSerializer<Date> {

    //UWAGA dont thread safety
    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        String formattedDate = formatter.format(value);
        gen.writeString(formattedDate);
    }
}
