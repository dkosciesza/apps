package com.example.tutorial;

import com.example.tutorial.user.domain.UserEntity;
import com.example.tutorial.user.domain.UserService;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Dominik on 2016-02-29.
 */
@Controller
public class TutorialMainViewController {

    private static final String PAGE = "index";
    @Autowired
    private UserService userService;

    @RequestMapping("/tutorial")
    public String mainTutorialPage() {
        return PAGE;
    }

    /**
     * Pobierz wszystkich uzytkownikow
     */
    @RequestMapping(value = "/tutorial/usersall", method = RequestMethod.GET)
    @ResponseBody
    public List<UserEntity> requestAllUsers() {
        return userService.findAll();
    }

    /**
     * Znajdz uzytkownika przez id
     */
    @RequestMapping(value = "/tutorial/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserEntity requestGetUser(@PathVariable("id") Long id) {
        return userService.findOne(id);
    }

    /**
     * Usun uzytkownika przez id
     */
    @RequestMapping(value = "/tutorial/user/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ImmutableMap<HttpStatus, String> requestRemoveUser(@PathVariable("id") Long id) {
        return userService.removeUser(id);
    }

    /**
     * Dodaj / zmodyfikuj uzytkownika
     */
    @RequestMapping(value = "/tutorial/user/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ImmutableMap<HttpStatus, String> requestAddUpdateUser( @RequestBody UserEntity user) throws ParseException {
        return userService.addUpdateUser(user);
    }

}
