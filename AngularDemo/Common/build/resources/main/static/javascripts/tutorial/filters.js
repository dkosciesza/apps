tutorialApp.filter('userfilter', function() {
    function strStartsWith(str, prefix) {
        return str.toLowerCase().indexOf(prefix.toLowerCase()) === 0;
    }

    return function(items, search) {
        // nie ustawiono filtru
        if (!search || !search.surname && !search.birthDate) {
            return items;
        }
        // nie uwzglednia surname AND birthDate
        if(search.surname){
            return items.filter(function(element, index, array) {
                return strStartsWith(element.surname, search.surname);
            });
        }
        if(search.birthDate){
            return items.filter(function(element) {
                return element.birthDate == search.birthDate;
            });
        }

    };
});