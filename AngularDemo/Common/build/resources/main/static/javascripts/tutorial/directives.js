tutorialApp.directive('jqDatePicker', ['$parse', function($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            var ngModel = $parse(attrs.ngModel);
            $(element).datepicker({
                dateFormat: 'yy/mm/dd',
                maxDate: new Date(),
                onSelect: function(dateText) {
                    scope.$apply(function(scope){
                        // update scope
                        ngModel.assign(scope, dateText);
                    });
                }
            });
        }
    };
}]);