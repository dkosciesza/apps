var tutorialApp = angular.module('tutorialApp', ['ngRoute', 'ngResource']);

// konfiguracja routingu
tutorialApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        // route for the home page
        .when('/users', {
            templateUrl : 'resource/pages/tutorial/users.html',
            controller  : 'usersController'
        })
        .when('/user', {
            templateUrl : 'resource/pages/tutorial/userForm.html',
            controller  : 'usersController'
        })
        .when('/features', {
            templateUrl : 'resource/pages/tutorial/features.html',
            controller  : 'featuresController'
        }).
        otherwise({
            redirectTo: '/index'
        });
}])
.controller('usersController', ['$scope', '$location', 'httpService', 'usersService','userAddUpdateService', 'usersAllUrlConfig', 'userGetService',
                                function($scope, $location, httpService, usersService, userAddUpdateService, usersAllUrlConfig, userGetService) {

        // zmienna nie obserwowana
        var vm = this;

        vm.getUsers = function() {
            httpService.getData(usersAllUrlConfig)
                .then(function(data) { // promise success
                    // zmienna obserwowana
                    $scope.users = data;
                    console.info(data);
                },
                function(error) { // promise error
                    $scope.users = [];
                    console.warn(error);
                });
        };
        vm.getUsers();

        //CRUD przyklad
        var getUser = function(userId){
            userGetService.getUser(userId)
                 .then(function(resp){
                    $scope.user = resp;
                },
                function(error) {
                    console.warn(error)
                });
        }
        $scope.addUpdateUser = function(){
            var res = userAddUpdateService.createUser($scope.user);
            res.$promise.then(function(response){
                // nieaktualny cache
                httpService.setActual(false);
                httpService.getData(usersAllUrlConfig).then(function(data) {
                    $scope.users = data;
                });
                $location.path("/users");
            },
            function(error) {
                console.warn(error);
            });
        }
        $scope.removeUser = function(userId){
            usersService.delete({id:userId})
                .$promise.then(function(response){
                // nieaktualny cache
                httpService.setActual(false);
                httpService.getData(usersAllUrlConfig).then(function(data) {
                    $scope.users = data;
                });
            },
            function(error) {
                console.warn(error);
            });
        };

        $scope.addUserRedirect = function(){
            $location.path('/user');
        }

        $scope.editUserRedirect = function(userId){
            getUser(userId);
            $scope.user;
            $scope.addUserRedirect();
        }

    }])
.controller('featuresController', ['$scope', function($scope) {
        this.title = "jednostronne wiązanie";

        $scope.data = {
            message : 'wiązanie dwukierunkowe',
            currentDate : new Date(),
            list: [
                {value: 1, name: "Jeden grosz"},
                {value: 2, name: "Dwa grosze"},
                {value: 3, name: "Trzy grosze"},
                {value: 4, name: "Cztery grosze"}
            ]
        };

        $scope.$watch('data.message', function () {

           console.log(new Date().getTime()+' Wywołano data.message $watch');

        });

        $scope.printMsgOnChange = function(data){
            console.log('Chage select: '+data);
        }
}]);