/**
 * Przykład $http
 */
tutorialApp.factory('httpService', ['$http', '$q', function($http, $q){
    var service = {
      actual: false,
      data: [],
      getData: getData,
      setActual: setActual
    };
    return service;

    function getData(config) {
        var def = $q.defer();
        // pseudo cache
        if(service.actual && service.data.length > 0){
            def.resolve(service.data);
            return def.promise;
        }

        $http(config)
            .success(function (data, status) {
                service.data = data;
                service.actual = true;
                def.resolve(data);
            }).error(function (error, status) {
                service.actual = false;
                def.reject("Fail fetch data, error: "+error);
            });
        return def.promise;
    }
    function setActual(a){
        service.actual = a;
    }
}]);
//----------------------------------------------------------------------------
/**
 * Przykład $resource
 */
tutorialApp.service('userAddUpdateService', ['$resource', function($resource){
    return $resource('/tutorial/user/update', {}, {
        createUser: {method: 'POST'}
    });
}]);
tutorialApp.service('usersService', ['$resource', function($resource){
    return $resource('/tutorial/user/:id', {id: '@id'}, {
       get: {method: 'GET'},
       delete: {method: 'DELETE'}
    });
}]);

tutorialApp.factory('userGetService', ['usersService', '$q', function(usersService, $q){
    function FindUserService() {
        this.getUser = function (userId) {
            var deferredObject = $q.defer();
            usersService.get({id:userId}).$promise
                .then(function (result) {
                    deferredObject.resolve(result);
                }, function (errorMsg) {
                    deferredObject.reject(errorMsg);
                });

            return deferredObject.promise;
        };
    }
    return new FindUserService();
}]);